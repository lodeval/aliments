* Toggle display

Le code refactorisé fourni introduit  ~plats~ comme étant un
tableau d'objets. Chaque objet décrit un plat:

#+begin_src javascript
{ 
  name: "Paris brest", 
  weight: 100, 
  caloriesTotal: 341,
  calories: 341,
  lipides: 8,
  glucides: 33,
  proteines: 17 
}
#+end_src

Il est bien plus facile de créer une nouvelle représentation html à partir d'une
représentation en javascript plutôt qu'à partir d'éléments html, plus difficile
à exploiter.

* À vous:
En vous inspirant de la fonctionnalité implémentée, codez le comportement
suivant: «à l'appui sur la touche l, l'affichage passe en mode liste».

Mode liste:

#+begin_src html
<ul>
  <li class="plat" data-weight="100" data-calories="341">
    <p>Paris brest</p>
    <dl>
      <dt>calories</dt>
      <dd>341</dd>
      <dt>lipides</dt>
      <dd>8</dd>
      <dt>glucides</dt>
      <dd>33</dd>
      <dt>protéines</dt>
      <dd>17</dd>
    </dl>
  </li>
</ul>
#+end_src
